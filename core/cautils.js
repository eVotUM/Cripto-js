// eVotUM - Electronic Voting System
//
// cautils.py
//
// Cripto-9.0.0 - CA utils
//
// Copyright (c) 2016 Universidade do Minho
// Developed by André Baptista - Devise Futures, Lda. (andre.baptista@devisefutures.com)
// Reviewed by Ricardo Barroso - Devise Futures, Lda. (ricardo.barroso@devisefutures.com)
//
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//

var DEFAULT_KEY_BITS_SIZE = 2048;
var DEFAULT_CIPHER = "aes256";

//Cripto 9.1.1
evotum.cautils.generateRSAKeyPair = function(keyBitsSize) {
//  Generate the RSA key pair.
//  Notice that you should use evotum.cautils.pKeyToPEMPrivateKey() and evotum.cautils.pKeyToPEMPublicKey() to transform the
//  RSA key pair into the private and public key, respectively, in PEM format.
//
//  Args:
//      keyBitsSize (int): RSA key bit size
//  Returns:
//      RSAKeyPair (Forge object): RSA key pair
  if(keyBitsSize === undefined) {
    keyBitsSize = DEFAULT_KEY_BITS_SIZE;
  }
  return forge.pki.rsa.generateKeyPair(keyBitsSize);
}

//Cripto 9.2.1
evotum.cautils.generateCSR = function(pemPrivateKey, keyPassphrase, commonName, countryName, stateOrProvinceName, localityName, organizationName, organizationalUnitName) {
//  Generates the certificate request.
//
//  Args:
//      pemPrivateKey (PEM): private key in PEM format
//      keyPassphrase (str): passphrase to access the private key pemPrivateKey
//      commonName (str): common name (CN) for the certificate requests
//      countryName (str): two-letter country name (C) for the certificate requests
//      stateOrProvinceName (str): state or province name (ST) for the certificate request
//      localityName (str): locality name (L) for the certificate request
//      organizationName (str): organization name (O) for the certificate request
//      organizationalUnitName (str): organizational unit name (OU) for the certificate request
//  Returns:
//      { errorCode (int/null), pemCSR (PEM/null)}: If errorCode is null, returns certificate request
//          in PEM format. If errorCode is not null, pemCSR will be null.
//          The meaning of errorCode is the following:
//              1 - invalid private key format or incorrect passphrase
//              2 - invalid certificate parameters
  var x509Request = forge.pki.createCertificationRequest();

  var result = evotum.cautils.pemPrivateKeyToPKey(pemPrivateKey, keyPassphrase);
  var errorCode = result["errorCode"];
  var keyPair = result["pKey"];

  if (errorCode === 1) {
    return {"errorCode": 1, "pemCSR": null};
  }

  x509Request.publicKey = keyPair.publicKey;

  try {
    x509Request.setSubject([{
      name: "commonName",
      value: commonName
    }, {
      name: "countryName",
      value: countryName
    }, {
      shortName: "ST",
      value: stateOrProvinceName
    }, {
      name: "localityName",
      value: localityName
    }, {
      name: "organizationName",
      value: organizationName
    }, {
      shortName: "OU",
      value: organizationalUnitName
    }]);
  }
  catch (error) {
    return {"errorCode": 2, "pemCSR": null};
  }

  x509Request.sign(keyPair.privateKey, forge.md.sha256.create());

  return {"errorCode": null, "pemCSR": forge.pki.certificationRequestToPem(x509Request)};
}

//Cripto 9.3.1
evotum.cautils.signCSR = function(pemCACertificate, pemCAPrivateKey, keyPassphrase, pemCSR, serialNumber, notBefore, notAfter) {
//  Signs the certificate request with the CA private key, and issues the certificate.
//
//  Args:
//      pemCACertificate (PEM): CA certificate in PEM format
//      pemCAPrivateKey (PEM): CA private key in PEM format
//      keyPassphrase (str): passphrase to access the private key pemCAPrivateKey
//      pemCSR (PEM): certificate request in PEM format
//      serialNumber (int): certificate serial number (each certificate signed by a CA should have a different serial number)
//      notBefore (int): adjust the timestamp (in GMT) when the certificate starts being valid, in relative seconds (0 = now)
//      notAfter (int): adjust the timestamp (in GMT) when the certificate stops being valid, in relative seconds (0 = now)
//  Returns:
//      {errorCode (int/null), pemCertificate (PEM/null)}: If errorCode is null, returns certificate
//          in PEM format. If errorCode is not null, pemCertificate will be null.
//          The meaning of errorCode is the following:
//              1 - invalid CA certificate format
//              2 - invalid CA private key format or incorrect passphrase
//              3 - invalid certificate request format
//              4 - invalid serial number
//              5 - invalid timestamp (notBefore or notAfter)
  var result = evotum.cautils.pemPrivateKeyToPKey(pemCAPrivateKey, keyPassphrase);
  var errorCode = result["errorCode"];
  var caPrivateKey = result["pKey"];

  if (errorCode === 1) {
    return {"errorCode": 2, "pemCertificate": null};
  }

  var x509CACertificate = forge.pki.certificateFromPem(pemCACertificate);

  if (x509CACertificate === null) {
    return {"errorCode": 1, "pemCertificate": null};
  }

  var x509Request = forge.pki.certificationRequestFromPem(pemCSR);

  if (x509Request === null) {
    return {"errorCode": 3, "pemCertificate": null};
  }

  var x509Certificate = forge.pki.createCertificate();
  x509Certificate.publicKey = x509Request.publicKey;

  try {
    x509Certificate.serialNumber = serialNumber;
  }
  catch (error) {
    return {"errorCode": 4, "pemCertificate": null};
  }

  try {
    var currentDate = new Date();

    var notBeforeDate = new Date();
    notBeforeDate.setSeconds(currentDate.getSeconds() + notBefore);
    var notAfterDate = new Date();
    notAfterDate.setSeconds(currentDate.getSeconds() + notAfter);

    x509Certificate.validity.notBefore = notBeforeDate;
    x509Certificate.validity.notAfter = notAfterDate;
  }
  catch (error) {
    return {"errorCode": 5, "pemCertificate": null};
  }

  x509Certificate.setIssuer(x509CACertificate.subject.attributes);
  x509Certificate.setSubject(x509Request.subject.attributes);

  x509Certificate.sign(caPrivateKey, forge.md.sha256.create());

  return {"errorCode": null, "pemCertificate": forge.pki.certificateToPem(x509Certificate)};
}

//Cripto 9.4.1
evotum.cautils.pKeyToPEMPrivateKey = function(keyPair, keyPassphrase, cipher) {
//  Taking the given key pair (keyPair), encapsulates its private key on an encrypted PEM
//  format, encrypted using cipher algorithm and protected with keyPassphrase passphrase.
//
//  Args:
//      keyPair (PKey object): RSA key pair
//      keyPassphrase (str): passphrase used to cipher the private key
//      cipher (str): cipher algorithm to be used to cipher the private key
//  Returns:
//      {errorCode (int/null), pemPrivateKey (PEM/null)}: If errorCode is null, returns
//          private key in PEM format. If errorCode is not null, pemPrivateKey will be null.
//          The meaning of errorCode is the following:
//              1 - invalid key pair.
  if(cipher === undefined) {
     cipher = DEFAULT_CIPHER;
  }
  try {
    var privateKey = keyPair.privateKey;
  }
  catch (error) {
    return {"errorCode": 1, "pemPrivateKey": null};
  }

  var rsaPrivateKey = forge.pki.privateKeyToAsn1(privateKey);
  var privateKeyInfo = forge.pki.wrapRsaPrivateKey(rsaPrivateKey);
  var encryptedPrivateKeyInfo = forge.pki.encryptPrivateKeyInfo(
    privateKeyInfo, keyPassphrase, {
      algorithm: cipher,
    });
  return {"errorCode": null, "pemPrivateKey": forge.pki.encryptedPrivateKeyToPem(encryptedPrivateKeyInfo)};
}

//Cripto 9.5.1
evotum.cautils.pKeyToPEMPublicKey = function(keyPair) {
//  Extracts the public key from key pair (keyPair) in PEM format.
//
//  Args:
//      keyPair (PKey object): RSA key pair
//  Returns:
//      {errorCode (int/null), pemPublicKey (PEM/null)}: If errorCode is null, returns
//          public key in PEM format. If errorCode is not null, pemPublicKey will be null.
//          The meaning of errorCode is the following:
//              1 - invalid key pair.
  try {
    var publicKey = keyPair.publicKey;
    return {"errorCode": null, "pemPublicKey": forge.pki.publicKeyToPem(publicKey)};
  }
  catch (error) {
    return {"errorCode": 1, "pemPublicKey": null};
  }
}

//Cripto 9.6.1
evotum.cautils.pemPrivateKeyToPKey = function(pemPrivateKey, keyPassphrase) {
//  Transforms the private key in PEM format into the RSA key pair (Pkey object).
//
//  Args:
//      pemPrivateKey (PEM): private key in PEM format
//      keyPassphrase (str): passphrase used to decipher the private key
//  Returns:
//      {errorCode (int/null), pKey (Forge object/null)}: If errorCode is null, returns
//          key pair. If errorCode is not null, pKey will be null.
//          The meaning of errorCode is the following:
//              1 - invalid private key or passphrase.
  var pKey = forge.pki.decryptRsaPrivateKey(pemPrivateKey, keyPassphrase);
  if (pKey === null) {
    return {"errorCode": 1, "pKey": null};
  }
  else {
    return {"errorCode": null, "pKey": pKey};
  }
}
