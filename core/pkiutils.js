// eVotUM - Electronic Voting System
//
// pkiutils.js
//
// Cripto-5.0.0 - Public Key cryptography Functions
//
// Copyright (c) 2016 Universidade do Minho
// Developed by André Baptista - Devise Futures, Lda. (andre.baptista@devisefutures.com)
// Reviewed by Ricardo Barroso - Devise Futures, Lda. (ricardo.barroso@devisefutures.com)
//
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//

var AES_KEY_BYTES_LENGTH = 32
var AES_BLOCK_SIZE = 16

//Cripto 5.1.2
evotum.pkiutils.getPublicKeyFromCertificate = function(pemCertificate) {
//  Retrieves public key from digital certificate in PEM format.
//
//  Args:
//      pemCertificate (pem): digital certificate in PEM format
//  Returns:
//      errorCode (int/null), publicKey (pem/null): json with error code and public key in
//        PEM format.
//        The errorCode has the following meaning:
//            null - the public key was retrieved and the certificate was within the validity period
//            1 - the public key was retrieved but the certificate is not within the validity period
//            2 - the certificate format is invalid (not PEM)
//            3 - it was not possible to retrieve the public key from the certificate
//        If the errorCode is 2 or 3, publicKey will be null
  try {
    var x509Certificate = forge.pki.certificateFromPem(pemCertificate, true);

    var currentTime = new Date();
    var expiredCertificate = (x509Certificate.validity.notAfter < currentTime);
  }
  catch (error) {
    return {"errorCode": 2, "publicKey": null};
  }

  var publicKey = x509Certificate.publicKey;

  if (publicKey != null) {
    var pemPublicKey = forge.pki.publicKeyToPem(publicKey);
    if (!expiredCertificate) {
      return {"errorCode": null, "publicKey": pemPublicKey};
    }
    else if (expiredCertificate) {
      return {"errorCode": 1, "publicKey": pemPublicKey};
    }
  }
  else {
    return {"errorCode": 3, "publicKey": null};
  }
}

//Cripto 5.2.2
evotum.pkiutils.encryptWithPublicKey = function(plaintext, pemCertificate) {
//  Encrypts plaintext with the public key in pemCertificate (digital certificate in PEM format).
//
//  Args:
//      plaintext (str): string to be ciphered
//      pemCertificate (pem): digital certificate in PEM format
//  Returns:
//      encObject (base64/null): encrypted payload. If it is not possible to
//      retrieve the public key form pemCertificate, the return value will be null.
  var result = evotum.pkiutils.getPublicKeyFromCertificate(pemCertificate);
  var pemPublicKey = result["publicKey"];

  if (pemPublicKey !== null) {
    var key = forge.random.getBytesSync(AES_KEY_BYTES_LENGTH);
    var iv = forge.random.getBytesSync(AES_BLOCK_SIZE);
    var cipher = forge.cipher.createCipher("AES-CBC", key);
    cipher.start({iv: iv});
    cipher.update(forge.util.createBuffer(plaintext));
    cipher.finish();
    var ciphertext = cipher.output.data;

    var publicKey = forge.pki.publicKeyFromPem(pemPublicKey, true);
    var encryptedKey = publicKey.encrypt(key, "RSA-OAEP", {
      md: forge.md.sha256.create()
    });
    encObjectStr = forge.util.encode64(encryptedKey) + "," + forge.util.bytesToHex(iv) + "," + forge.util.encode64(ciphertext);
    return forge.util.encode64(encObjectStr);
  }

  return null;
}

//Cripto 5.3.2
evotum.pkiutils.verifyCertificateChain = function(pemCertificate, rootPEMCertificate) {
//  Verify if pemCertificate (digital certificate in PEM format) has been signed by
//  rootPEMCertificate (Certification Authority digital certificate in PEM format).
//  Note: This function doesn't validate if pemCertificate or rootPEMCertificate are
//  well formed digital certificates in PEM format. Please use other functions to validate.
//
//  Args:
//      pemCertificate (pem): digital certificate in PEM format
//      rootPEMCertificate (pem): Certification Authority digital certificate in PEM format
//  Returns:
//      boolean: True if pemCertificate was signed by rootPEMCertificate; False otherwise.
  var x509Certificate = forge.pki.certificateFromPem(pemCertificate);
  var rootX509Certificate = forge.pki.certificateFromPem(rootPEMCertificate);

  caStore = forge.pki.createCaStore();
  caStore.addCertificate(rootPEMCertificate);

  var chain = [x509Certificate, rootX509Certificate]

  try {
    forge.pki.verifyCertificateChain(caStore, chain);
    return true;
  }
  catch (error) {
    return false;
  }
}
